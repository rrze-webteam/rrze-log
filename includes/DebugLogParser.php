<?php

namespace RRZE\Log;

defined('ABSPATH') || exit;

class DebugLogParser
{
    /**
     * [protected description]
     * @var object
     */
    protected $options;

    /**
     * [protected description]
     * @var mixed null|\WP_Error
     */
    protected $error = null;

    /**
     * [protected description]
     * @var mixed null|\SplFileObject
     */
    protected $file = null;

    protected $maxFileSize = 31457280; // 30Mb

    /**
     * [protected description]
     * @var array
     */
    protected $search;

    /**
     * [protected description]
     * @var integer
     */
    protected $offset;

    /**
     * [protected description]
     * @var integer
     */
    protected $count;

    /**
     * [protected description]
     * @var integer
     */
    protected $totalLines = 0;

    /**
     * [__construct description]
     * @param string  $filename [description]
     * @param array   $search   [description]
     * @param integer $offset   [description]
     * @param integer $count    [description]
     */
    public function __construct($filename, $search = [], $offset = 0, $count = -1)
    {
        $this->offset = $offset;
        $this->count = $count;
        $search = array_map('mb_strtolower', $search);
        $this->search = array_filter($search);

        if (!file_exists($filename)) {
            $this->error = new \WP_Error('rrze_log_file', __('Log file not found.', 'rrze-log'));
        } else {
            if (filesize($filename) > $this->maxFileSize) {
                return false;
            }
            $this->file = new \SplFileObject($filename);
            $this->file->setFlags(
                \SplFileObject::READ_AHEAD |
                    \SplFileObject::SKIP_EMPTY
            );
        }
    }

    public function getItems()
    {
        if (is_wp_error($this->error)) {
            return $this->error;
        }
        $errors = $this->processFileContent();
        $lines = [];
        foreach ($errors as $line) {
            $searchStr = json_encode($line);
            if (!$this->search || $this->search($searchStr)) {
                $details = explode('@@@', $line['details']);
                $lines[] = [
                    'level' => $line['level'],
                    'message' => $details[0],
                    'datetime' => $line['occurrences'][0],
                    'details' => $details,
                    'ocurrencies' => count($line['occurrences'])
                ];
            }
        }
        $this->totalLines = count($lines);
        if (count($lines) >= $this->offset) {
            $limitIterator = new \LimitIterator(new \ArrayIterator($lines), $this->offset, $this->count);
        } else {
            $limitIterator = new \LimitIterator(new \ArrayIterator([]));
        }
        return $limitIterator;
    }

    /**
     * [search description]
     * @param  string $haystack [description]
     * @return boolean           [description]
     */
    protected function search($haystack)
    {
        $find = true;
        $haystack = mb_strtolower($haystack);
        foreach ($this->search as $needle) {
            if (is_array($needle) && !empty($needle)) {
                foreach ($needle as $str) {
                    if (mb_stripos($haystack, $str) === false) {
                        $find = $find && false;
                    } else {
                        $find = $find && true;
                    }
                }
            } else {
                if (mb_stripos($haystack, $needle) === false) {
                    $find = $find && false;
                }
            }
        }
        return $find;
    }

    protected function processFileContent()
    {
        // Read the errors log file
        $logSize = is_object($this->file) ? $this->file->getSize() : null;
        if (!$logSize) {
            return [];
        }

        $content = $this->file->fread($logSize);
        $pattern = '/^\[(.*UTC)\]\s/mi';
        $splitedLine = preg_split($pattern, $content, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
        $lines = [];
        foreach (array_keys($splitedLine) as $key) {
            if ($key % 2 == 0) {
                $lines[$key] = $splitedLine[$key + 1];
            }
        }
        // Pluck out the last 100k entries, the newest entry is last.
        $lines = array_slice($lines, -100000, null, true);

        $prependLines = [];
        foreach ($lines as $key => $line) {
            // Replace ABSPATH
            $line = str_replace(ABSPATH, ".../", $line);

            // Add '@@@' as marker/separator before Stack trace.
            $line = str_replace("Stack trace:", "@@@Stack trace:", $line);

            if (strpos($line, 'PHP Fatal') !== false) {
                // Add '@@@' as marker/separator on PHP Fatal error's stack trace lines.
                $line = str_replace("#", "@@@#", $line);
            }

            // Remove @@@ on certain error messages.
            $line = str_replace("Argument @@@#", "Argument #", $line);
            $line = str_replace("parameter @@@#", "parameter #", $line);
            $line = str_replace("the @@@#", "the #", $line);

            $prependLines[$key] = $line;
        }

        // Reverse the order of the entries, so the newest entry is first.
        $latestLines = array_reverse($prependLines, true);

        // Will hold error details types
        $errorList = [];

        foreach ($latestLines as $key => $error) {
            $timestamp = $splitedLine[$key];
            //$errorFile = '';

            if ((false !== strpos($error, 'PHP Fatal')) || (false !== strpos($error, 'FATAL')) || (false !== strpos($error, 'E_ERROR'))) {
                $errorLevel = 'FATAL';
                $errorDetails = str_replace("PHP Fatal error: ", "", $error);
                $errorDetails = str_replace("PHP Fatal: ", "", $errorDetails);
                $errorDetails = str_replace("FATAL ", "", $errorDetails);
                $errorDetails = str_replace("E_ERROR: ", "", $errorDetails);
            } elseif ((false !== strpos($error, 'PHP Warning')) || (false !== strpos($error, 'E_WARNING'))) {
                $errorLevel = 'WARNING';
                $errorDetails = str_replace("PHP Warning: ", "", $error);
                $errorDetails = str_replace("E_WARNING: ", "", $errorDetails);
            } elseif ((false !== strpos($error, 'PHP Notice')) || (false !== strpos($error, 'E_NOTICE'))) {
                $errorLevel = 'NOTICE';
                $errorDetails = str_replace("PHP Notice: ", "", $error);
                $errorDetails = str_replace("E_NOTICE: ", "", $errorDetails);
            } elseif (false !== strpos($error, 'PHP Deprecated')) {
                $errorLevel = 'DEPRECATED';
                $errorDetails = str_replace("PHP Deprecated: ", "", $error);
            } elseif ((false !== strpos($error, 'PHP Parse')) || (false !== strpos($error, 'E_PARSE'))) {
                $errorLevel = 'PARSE';
                $errorDetails = str_replace("PHP Parse error: ", "", $error);
                $errorDetails = str_replace("E_PARSE: ", "", $errorDetails);
            } elseif (false !== strpos($error, 'EXCEPTION:')) {
                $errorLevel = 'EXCEPTION';
                $errorDetails = str_replace("EXCEPTION: ", "", $error);
            } elseif (false !== strpos($error, 'WordPress database error')) {
                $errorLevel = 'DATABASE';
                $errorDetails = str_replace("WordPress database error ", "", $error);
            } elseif (false !== strpos($error, 'JavaScript Error')) {
                $errorLevel = 'JAVASCRIPT';
                $errorDetails = str_replace("JavaScript Error: ", "", $error);
            } else {
                $errorLevel = 'OTHER';
                $errorDetails = $error;
                if (Utils::isJson($errorDetails)) {
                    // For JSON string in error message, originally added via error_log(json_encode($variable))
                    // This will output said JSON string as well-formated array in the log entries table
                    $errorDetails = print_r(json_decode($errorDetails, true), true);
                }
            }

            $errorDetails = trim(preg_replace('/([\r\n\t])/', '', wp_kses_post($errorDetails)));

            if (array_search(trim($errorDetails), array_column($errorList, 'details')) === false) {
                $errorList[] = [
                    'level' => $errorLevel,
                    'details' => $errorDetails,
                    'occurrences' => [$timestamp],
                ];
            } else {
                $errorPosition = array_search($errorDetails, array_column($errorList, 'details'));
                array_push($errorList[$errorPosition]['occurrences'], $timestamp);
            }
        }

        return $errorList;
    }

    /**
     * [getTotalLines description]
     * @return integer [description]
     */
    public function getTotalLines()
    {
        return $this->totalLines;
    }
}
