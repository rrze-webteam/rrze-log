��            )         �     �     �     �     �     �  
   �       4        T     q  	   }  C   �  Z   �  Y   &     �     �     �     �     �     �     �  �   �  7   t  V   �  b        f     s  ;   {      �  0   �  �  	     �  (   �     �     �     �     	  !   	  9   8	  +   r	     �	  
   �	  ^   �	  W   
  {   l
  	   �
     �
  	             0     D     S  �   `  6     Y   ?  e   �     �       I        ^  0   k         	                                                                                                          
                     All error levels Could not get lock on %s. Date Debug Debug Log Access Enable Log Enable administration menus Enables network wide the Log menu for administrators Enables network-wide logging Error level Error: %s How many days can the log file remain on disk before it is removed? Invalid value of the WP_DEBUG_LOG constant. WP_DEBUG_LOG must have the following value: %s List of usernames with access to view the wp debug log file. Enter one username per line. Log Log file not found. Message Number of items per page: Plugins: %1$s: %2$s RRZE Log RRZE Webteam The plugin allows you to log certain actions of the plugins and themes in a log file, which are or may be necessary for further investigations. The plugin is compatible only with WordPress Multisite. The server is running PHP version %1$s. The Plugin requires at least PHP version %2$s. The server is running WordPress version %1$s. The Plugin requires at least WordPress version %2$s. Time to live Website You do not have sufficient permissions to access this page. https://blogs.fau.de/webworking/ https://gitlab.rrze.fau.de/rrze-webteam/rrze-log Project-Id-Version: RRZE Log
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/rrze-log
PO-Revision-Date: 2023-03-21 17:05+0100
Last-Translator: RRZE Webteam <webmaster@fau.de>
Language-Team: Deutsch
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 3.2.2
X-Loco-Version: 2.3.1; wp-5.3.2
 Alle Fehlerstufen Konnte die Sperre auf %s nicht erhalten. Datum Debug Debug-Log-Zugriff Protokoll aktivieren Administrations-Menüs aktivieren Aktiviert netzwerkweit das Log-Menü für Administratoren Aktiviert die netzwerkweite Protokollierung Fehlerstufe Fehler: %s Wie viele Tage kann die Protokolldatei auf der Festplatte verbleiben, bevor sie entfernt wird? Ungültiger Wert der Konstante WP_DEBUG_LOG. WP_DEBUG_LOG muss folgenden Wert haben: %s Liste der Benutzernamen mit Zugriff zum Anzeigen der Debugging-Protokolldatei. Geben Sie einen Benutzernamen pro Zeile ein. Protokoll Protokolldatei nicht gefunden. Nachricht Einträge pro Seite: Plugins: %1$s: %2$s RRZE Protokoll RRZE-Webteam Das Plugin erlaubt es, bestimmte Aktionen der Plugins und Themes in einer Logdatei zu protokollieren, die für weitere Untersuchungen notwendig sind oder sein können. Das Plugin ist nur mit WordPress Multisite kompatibel. Auf dem Server läuft PHP Version %1$s. Das Plugin benötigt mindestens PHP Version %2$s. Auf dem Server läuft WordPress Version %1$s. Das Plugin benötigt mindestens WordPress Version %2$s. Time to live Website Sie haben nicht genügend Berechtigungen, um auf diese Seite zuzugreifen. RRZE-Webteam https://gitlab.rrze.fau.de/rrze-webteam/rrze-log 